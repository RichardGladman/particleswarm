package main

import (
	"math/rand"
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/RichardGladman/particleswarm/configuration"
	"gitlab.com/RichardGladman/particleswarm/particles"
	"gitlab.com/RichardGladman/particleswarm/view"
)

func main() {

	config := configuration.Flags{}
	configuration.Parse(&config)

	rand.Seed(time.Now().UnixNano())
	s := particles.NewSwarm(&config)

	if view.Init(config) {
		defer view.Close()

		running := true
		for running {

			elapsed := sdl.GetTicks()
			interval := elapsed - particles.ElapsedTime

			r, g, b := s.SetColour(elapsed)
			s.Update(r, g, b, interval)
			view.BoxBlur()

			view.Update()

			running = view.HandleEvents()

			particles.ElapsedTime = elapsed

			if (1000 / uint32(config.Fps)) > sdl.GetTicks()-elapsed {
				sdl.Delay(1000 / uint32(config.Fps))
			}
		}
	}
}
