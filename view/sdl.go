package view

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/RichardGladman/particleswarm/configuration"
)

var window *sdl.Window
var renderer *sdl.Renderer
var texture *sdl.Texture

var buffer1 []uint32
var buffer2 []uint32

var config configuration.Flags

// Init : Initialises and set up the SDL environment.
func Init(c configuration.Flags) bool {

	config = c

	mw := int32(config.WindowWidth)
	mh := int32(config.WindowHeight)

	w, err := sdl.CreateWindow("Go Particles Go!", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, mw, mh, sdl.WINDOW_SHOWN)
	if err != nil {
		Close()
		return false
	}

	window = w

	r, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_PRESENTVSYNC)
	if err != nil {
		Close()
		return false
	}

	renderer = r

	t, err := renderer.CreateTexture(sdl.PIXELFORMAT_RGBA8888, sdl.TEXTUREACCESS_STATIC, mw, mh)
	if err != nil {
		Close()
		return false
	}

	texture = t

	buffer1 = make([]uint32, config.WindowWidth*config.WindowHeight)
	buffer2 = make([]uint32, config.WindowWidth*config.WindowHeight)

	window.SetBrightness(float32(config.WindowBrightness))

	return true

}

// HandleEvents : Top level event handler. In this case, only quit.
func HandleEvents() bool {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch event.(type) {
		case *sdl.QuitEvent:
			return false
		}
	}
	return true
}

// SetPixel : Sets an individual pixel to specified colour
func SetPixel(x int, y int, r uint32, g uint32, b uint32) {

	mw := int(config.WindowWidth)
	mh := int(config.WindowHeight)

	if x < 0 || x > mw || y < 0 || y > mh {
		return
	}

	var colour uint32
	colour += r
	colour <<= 8
	colour += g
	colour <<= 8
	colour += b
	colour <<= 8
	colour += 0xFF

	buffer1[(y*mw)+x] = colour
}

// BoxBlur : Blurs each particle by averaging surrounding pixels.
func BoxBlur() {
	temp := buffer1
	buffer1 = buffer2
	buffer2 = temp

	mw := int(config.WindowWidth)
	mh := int(config.WindowHeight)

	for y := 0; y < mh; y++ {
		for x := 0; x < mw; x++ {
			var redTotal uint8
			var blueTotal uint8
			var greenTotal uint8

			for row := -1; row <= 1; row++ {
				for col := -1; col <= 1; col++ {
					currentX := x + col
					currentY := y + row
					if currentX >= 0 && currentX < mw && currentY >= 0 && currentY < mh {
						colour := uint32(buffer2[currentY*mw+currentX])
						r := uint8(colour >> 24)
						g := uint8(colour >> 16)
						b := uint8(colour >> 8)
						redTotal += r
						greenTotal += g
						blueTotal += b
					}
				}
			}

			red := redTotal / 9
			blue := blueTotal / 9
			green := greenTotal / 9
			SetPixel(x, y, uint32(red), uint32(green), uint32(blue))
		}
	}
}

// Update : draw to the screen.
func Update() {

	mw := int(config.WindowWidth)
	texture.UpdateRGBA(nil, buffer1, int(mw))
	renderer.Clear()
	renderer.Copy(texture, nil, nil)
	renderer.Present()

}

// Close : Tear down the SDL environment in a friendly way.
func Close() {

	if renderer != nil {
		renderer.Destroy()
	}

	if texture != nil {
		texture.Destroy()
	}

	if window != nil {
		window.Destroy()
	}

	sdl.Quit()
}

// MinColour : Sets minimum allowed colour component value
func MinColour(cv uint32) uint32 {
	if cv < 10 {
		cv += 10
	}

	return cv

}
