# Go Particles Go

A simple particle swarm written in go using SDL. Currently has a deliberately naive blur implementation.

## To do

* ~~Control various settings from command line~~ Done
* Export as gif file
* Use go-routines to process pixels and/or blur
* Control settings from within the software
* Rewrite blur to me much more efficient
* Actual gravity physics rather than the cheat I use now.

## Getting and building

Once cloned, simply run the following in the particleswarm directory:

    go build
    ./particleswarm

You will also need the go SDL bindings from https://github.com/veandco/go-sdl2/sdl along with any of the development dependencies mentioned in the readme.

## Running and playing

Running go particle go is as easy as executing `./particleswarm` in the project directory. You should see a particle swarm created with some reasonable defaults. On some systems, you may not see any particles. In such a case use the brightness flag to set the brightness.

-brightness -br *type float, default 1.0*

Sets the brightness of the window. A value of 1.0 leaves the brightness unaffected. This will adjust the brightness of the entire display while the window has focus. Thanks SDL :)

-particles -p *type int, default 2,500*

How many particles in the swarm. Values of 500 and up give pretty cool results. Values of 5,000 to 15,000 can ve very trippy but higher values can cause some flickering in the center of the swarm.

-red -r, -green -g, -blue -b *type int default 5*

The red, green and blue colour components of the particles. These may not do what you'd expect. The values range from 0 to 255 and on some screens larger values work better while on others, smaller work better. This is probably due to a bug I haven't tracked down yet!

-gravity -gr *type float default 0.05*

Controls the gravity strength.

-gravitytype -gt *type int default 1*

Select the gravity type. 1: Sinic, 2: Cosinic, 3: Tanic

-speed -s *type float default 0.25*

Controls how fast the particles move.

-fps *type int default 50 *

Sets the traget frame rate

