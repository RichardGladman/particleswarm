package configuration

import (
	"flag"
)

const (
	width               = 800
	widthHelpText       = "The width of the program window"
	height              = 600
	heightHelpText      = "The height of the window"
	numParticles        = 2500
	particleHelpText    = "How many particles to drawand a newline"
	brightness          = 1
	brightnessHelpText  = "On some screens, the display is to dark. Set the brightness to between 1 and 2 to compensate"
	colour              = 5
	colourHelpText      = "Set the pixel swarm starting colour"
	gravity             = 0.05
	gravityHelpText     = "Sets the particle attraction rate"
	gravityType         = 1
	gravityTypeHelpText = "Select the gravity type. 1: Sinic, 2: Cosinic, 3: Tanic"
	speed               = 0.250
	speedHelpText       = "The speed of the particles."
	fps                 = 50
	fpsHelpText         = "You should know what FPS is"
)

// Flags : Exports the confiruation flags.
type Flags struct {
	WindowWidth      int
	WindowHeight     int
	WindowBrightness float64
	ParticlesNumber  int
	ParticleRed      uint64
	ParticleGreen    uint64
	ParticleBlue     uint64
	ParticleGravity  float64
	ParticleSpeed    float64
	SwarmGravity     int
	Fps              int
}

// Parse : Parses the command line flags into the congiguration structure.
func Parse(f *Flags) bool {

	flag.IntVar(&f.WindowWidth, "width", width, widthHelpText)
	flag.IntVar(&f.WindowWidth, "w", width, widthHelpText+" (shorthand)")
	flag.IntVar(&f.WindowHeight, "height", height, heightHelpText)
	flag.IntVar(&f.WindowHeight, "h", height, heightHelpText+" (shorthand)")
	flag.Float64Var(&f.WindowBrightness, "brightness", brightness, brightnessHelpText)
	flag.Float64Var(&f.WindowBrightness, "br", brightness, brightnessHelpText+" (shorthand)")

	flag.IntVar(&f.ParticlesNumber, "particles", numParticles, particleHelpText)
	flag.IntVar(&f.ParticlesNumber, "p", numParticles, particleHelpText+" (shorthand)")
	flag.Float64Var(&f.ParticleGravity, "gravity", gravity, gravityHelpText)
	flag.Float64Var(&f.ParticleGravity, "gr", gravity, gravityHelpText+" (shorthand)")

	flag.Uint64Var(&f.ParticleRed, "red", colour, colourHelpText+" (Red)")
	flag.Uint64Var(&f.ParticleGreen, "green", colour, colourHelpText+" (Green)")
	flag.Uint64Var(&f.ParticleBlue, "blue", colour, colourHelpText+" (Blue)")
	flag.Uint64Var(&f.ParticleRed, "r", colour, colourHelpText+" (Red shorthand)")
	flag.Uint64Var(&f.ParticleGreen, "g", colour, colourHelpText+" (Green shorthand)")
	flag.Uint64Var(&f.ParticleBlue, "b", colour, colourHelpText+" (Blue shorthand)")
	flag.Float64Var(&f.ParticleSpeed, "speed", speed, speedHelpText)
	flag.Float64Var(&f.ParticleSpeed, "s", speed, speedHelpText)

	flag.IntVar(&f.SwarmGravity, "gravitytype", gravityType, gravityTypeHelpText)
	flag.IntVar(&f.SwarmGravity, "gt", gravityType, gravityTypeHelpText+" (shorthand)")

	flag.IntVar(&f.Fps, "fps", fps, fpsHelpText)

	flag.Parse()

	return true
}
