package particles

import (
	"math"
	"math/rand"

	"gitlab.com/RichardGladman/particleswarm/configuration"
	"gitlab.com/RichardGladman/particleswarm/view"
)

var config *configuration.Flags

var ElapsedTime uint32

// Particle : Defines a single particle within the swarm.
type Particle struct {
	X         float64
	Y         float64
	speed     float64
	direction float64
}

// Swarm : Array of particles.
type Swarm []*Particle

//SetColour : Sets the colour of the swarm over time
func (s Swarm) SetColour(et uint32) (uint32, uint32, uint32) {

	var i float64

	switch config.SwarmGravity {
	case 1:
		i = (2 + math.Sin(float64(et)*0.00002))
		break
	case 2:
		i = (1 + math.Cos(float64(et)*0.00002))
		break
	case 3:
		i = (3 + math.Tan(float64(et)*0.00002))
		break
	}

	r := uint32(i+(255/float64(config.ParticleRed))) * 128
	g := uint32(i+(255/float64(config.ParticleGreen))) * 128
	b := uint32(i+(255/float64(config.ParticleBlue))) * 128

	r = view.MinColour(r % 255)
	g = view.MinColour(g % 255)
	b = view.MinColour(b % 255)

	return r, g, b
}

// Update : Wapper method to update all particles in the swarm.
func (s Swarm) Update(r uint32, g uint32, b uint32, e uint32) {
	for _, p := range s {
		p.UpdateParticle(e)
		view.SetPixel(int(p.X), int(p.Y), r, g, b)
	}
}

func (p *Particle) initialise() {
	p.X = float64(config.WindowWidth / 2.0)
	p.Y = float64(config.WindowHeight / 2.0)
	p.direction = 2 * math.Pi * rand.Float64()
	p.speed = config.ParticleSpeed * rand.Float64()
}

// UpdateParticle : update a single particle
func (p *Particle) UpdateParticle(interval uint32) {

	i := float64(interval)

	switch config.SwarmGravity {
	case 1:
		p.direction += math.Sin(1+i) * config.ParticleGravity
		break
	case 2:
		p.direction += math.Cos(1+i) * config.ParticleGravity
		break
	case 3:
		p.direction += math.Tan(1+i) * config.ParticleGravity
		break
	}

	xspeed := p.speed * math.Cos(p.direction)
	yspeed := p.speed * math.Sin(p.direction)

	p.X += (xspeed * i)
	p.Y += (yspeed * i)

	if p.X <= 0 || p.X >= float64(config.WindowWidth) || p.Y <= 0 || p.Y >= float64(config.WindowHeight) || rand.Intn(100) < 1 {
		p.initialise()
	}
}

func newParticle() *Particle {
	p := Particle{}
	p.initialise()
	return &p
}

// NewSwarm : Initialise the swarm of particles.
func NewSwarm(c *configuration.Flags) Swarm {

	config = c

	var particleSwarm = make(Swarm, config.ParticlesNumber)
	for i := range particleSwarm {
		particleSwarm[i] = newParticle()
	}

	return particleSwarm
}
